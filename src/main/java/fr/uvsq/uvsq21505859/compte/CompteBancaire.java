/**
 * @author ANDRIAMANGA
 * Classe simulant un compte bancaire
 */

package fr.uvsq.uvsq21505859.compte;

/**
 * @author herin
 *
 */
public class CompteBancaire {

	private int solde;

	/**
	 * Initialise le compte bancaire
	 * 
	 * @param soldeInitial
	 * @throws SoldeNegatifException
	 */
	public CompteBancaire(int soldeInitial) throws SoldeNegatifException {
		if (soldeInitial < 0) {
			throw new SoldeNegatifException();
		}
		this.solde = soldeInitial;
	}

	/**
	 * 
	 * @return solde
	 */

	public int SoldeCompte() {
		System.out.println("Votre solde est de:" + this.getSolde());
		return this.getSolde();
	}

	/**
	 * Permet de créditer un compte
	 * 
	 * @param crédit
	 * @throws SoldePosException
	 */
	public void crediterCompte(int crédit) throws SoldePosException {
		if (crédit < 0) {
			throw new SoldePosException();
		}
		this.ajoutCrédit(crédit);
	}

	/**
	 * Permet de débiter un compte
	 * 
	 * @param débit
	 */
	public void debiterCompte(int débit) { // ajouter exception si valeur negatif
		this.retirerDébit(débit);
	}

	/**
	 * Permet de faire un virement vers un autre compte
	 * 
	 * @param receveur
	 * @param montant
	 */
	public void virement(CompteBancaire receveur, int montant) { // ajouter exception si valeur negatif
		this.debiterCompte(montant);
		receveur.ajoutCrédit(montant);
	}

	// getters and setters
	/**
	 * getters solde
	 * 
	 * @return
	 */
	public int getSolde() {
		return this.solde;
	}

	/**
	 * setter solde
	 * 
	 * @param newSolde
	 * @throws SoldeNegatifException
	 */
	public void setSolde(int newSolde) throws SoldeNegatifException {
		if (newSolde < 0) {
			throw new SoldeNegatifException();
		}
		this.solde = newSolde;
	}

	/**
	 * Permet d'ajouter du crédit
	 * 
	 * @param crédit
	 */
	public void ajoutCrédit(int crédit) { // ajouter exception si valeur negatif
		this.setSolde(this.getSolde() + crédit);
	}

	/**
	 * @param débit
	 */
	public void retirerDébit(int débit) { // ajouter exception si valeur negatif

		this.setSolde(this.getSolde() - débit);
	}

}
