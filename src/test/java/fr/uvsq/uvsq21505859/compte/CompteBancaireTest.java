/**
 * @author ANDRIAMANGA Vahiniaina
 * Classe contenant les tests
 */

package fr.uvsq.uvsq21505859.compte;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author herin Effectue toutes les tests
 */
public class CompteBancaireTest {

	/**
	 * Initialise le compte test si le compte bancaire est bien initi�
	 */
	@Test
	public void testInitCompt() {
		CompteBancaire com = new CompteBancaire(100);
		assertEquals(100, com.getSolde());
	}

	/**
	 * test si le compte est bien initialiser
	 */
	@Test
	public void testSoldeCompte() {
		CompteBancaire com = new CompteBancaire(1000);
		assertEquals(1000, com.getSolde());
	}

	/**
	 * V�rifie si les op�rations de cr�dits effectuer sont correctes
	 */
	@Test
	public void testCreditCompte() {
		int crédit = 200;
		CompteBancaire com = new CompteBancaire(100);
		com.ajoutCrédit(crédit);
		assertEquals(crédit + 100, com.getSolde());
	}

	/**
	 * Teste si les op�rations de d�bit sont correctes
	 */
	@Test
	public void testDebiterCompte() {
		int débit = 300;
		CompteBancaire com = new CompteBancaire(1000);
		com.debiterCompte(débit);
		assertEquals(1000 - débit, com.getSolde());
	}

	/**
	 * Teste si les virements sont �xactement eff�ctuer
	 */
	@Test
	public void testVirement() {
		int montant = 300;
		CompteBancaire com = new CompteBancaire(1000);
		CompteBancaire rec = new CompteBancaire(0);
		com.virement(rec, montant);
		assertEquals(1000 - montant, com.getSolde());
	}

	/**
	 * Teste si un compte est � d�couvert
	 */
	@Test(expected = SoldeNegatifException.class)
	public void testDecouvert() {
		new CompteBancaire(-100); // Eviter d'utiliser une nouvelle variable qui ne sera pas utiliser
	}

	@Test(expected = SoldePosException.class)
	public void testSoldePis() {
		CompteBancaire com = new CompteBancaire(1000);
		com.crediterCompte(-1000);

	}

}
